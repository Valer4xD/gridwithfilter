﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GridWithFilter
{
    public class UserEntity : AbstractEntity<long>
    {
        public bool Active { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string MiddleName { get; set; }

        [DateOfBirth]
        public DateTime DateOfBirth { get; set; }

        public UserEntity(
            long id,
            bool active,
            string surname,
            string name,
            string middleName,
            DateTime dateOfBirth)
        {
            Id = id;
            Active = active;
            Surname = surname;
            Name = name;
            MiddleName = middleName;
            DateOfBirth = dateOfBirth;
        }
    }
}
