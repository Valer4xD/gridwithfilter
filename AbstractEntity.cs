﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GridWithFilter
{
    public class AbstractEntity<TId>
    {
        public TId Id { get; set; }

        public ICollection<ValidationResult> GetValidationResults()
        {
            ICollection<ValidationResult> validationResults = new List<ValidationResult>();
            var context = new ValidationContext(this);
            Validator.TryValidateObject(this, context, validationResults, true);
            return validationResults;
        }
    }
}
