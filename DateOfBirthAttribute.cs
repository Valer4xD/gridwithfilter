﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GridWithFilter
{
    public class DateOfBirthAttribute : RangeAttribute
    {
        public DateOfBirthAttribute()
            : base(typeof(DateTime), DateTime.Now.AddYears(-123).ToShortDateString(), DateTime.Now.AddMonths(-1).ToShortDateString()) {}
    }
}
