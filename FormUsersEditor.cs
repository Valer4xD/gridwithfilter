﻿using GridWithFilter.HelpersToControls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace GridWithFilter
{
    internal partial class FormUsersEditor : Form
    {
        private GridWithFilter<UserEntity> _gridUsers;

        private long idSerial = 0;
        private long CreateUserId() => idSerial ++;

        public FormUsersEditor()
        {
            InitializeComponent();

            ICollection<UserEntity> users = new List<UserEntity>();

            users.Add(new UserEntity
                (id: CreateUserId(), active: true, surname: "Исаев", name: "Валерий", middleName: "Юрьевич", dateOfBirth: new DateTime(1984, 05, 15)));
            users.Add(new UserEntity(CreateUserId(), false, "Александр", "Александров", "Александрович", new DateTime(1981, 09, 29)));
            users.Add(new UserEntity(CreateUserId(), true, "Чекина", "Елена", "Владимирова", new DateTime(1995, 05, 26)));
            users.Add(new UserEntity(CreateUserId(), true, "Петров", "Пётр", "Петрович", new DateTime(1990, 09, 03)));
            users.Add(new UserEntity(CreateUserId(), true, "Иванов", "Иван", "Иванович", new DateTime(1980, 07, 05)));
            users.Add(new UserEntity(CreateUserId(), true, "Васильев", "Василий", "Васильевич", new DateTime(1996, 04, 24)));
            users.Add(new UserEntity(CreateUserId(), true, "Дмитрий", "Дмитров", "Дмитриевич", new DateTime(1997, 08, 09)));
            users.Add(new UserEntity(CreateUserId(), true, "Фёдор", "Фёдоров", "Фёдорович", new DateTime(2001, 12, 13)));
            users.Add(new UserEntity(CreateUserId(), true, "Алексеев", "Алексей", "Алексеевич", new DateTime(2001, 12, 13)));
            users.Add(new UserEntity(CreateUserId(), true, "Михайлов", "Михаил", "Михайлович", new DateTime(2001, 12, 13)));
            users.Add(new UserEntity(CreateUserId(), true, "Владимир", "Владимиров", "Владимирович", new DateTime(2001, 12, 13)));

            List<GridWithFilter<UserEntity>.ColumnsSettings> columnsSettingsForUsers = new List<GridWithFilter<UserEntity>.ColumnsSettings>();

            columnsSettingsForUsers.Add(new GridWithFilter<UserEntity>.ColumnsSettings(fieldName: "Active", headerText: "Активен", 65));
            columnsSettingsForUsers.Add(new GridWithFilter<UserEntity>.ColumnsSettings("Surname", "Фамилия", 100));
            columnsSettingsForUsers.Add(new GridWithFilter<UserEntity>.ColumnsSettings("Name", "Имя", 100));
            columnsSettingsForUsers.Add(new GridWithFilter<UserEntity>.ColumnsSettings("MiddleName", "Отчество", 100));
            columnsSettingsForUsers.Add(new GridWithFilter<UserEntity>.ColumnsSettings("DateOfBirth", "Дата рождения", 100));

            _gridUsers = new GridWithFilter<UserEntity>(
                ref bindingSourceUsers,
                components,
                columnsSettingsForUsers,
                gridFilterHeaderHeight: 34,
                sortColumnID: 1);

            _gridUsers.InitializeComponent(
                "Пользователи",
                groupBoxLocation: new Point(0, 0),
                groupBoxHeight: 303,
                tabIndexFilter: 4,
                tabIndexData: 5);

            SuspendLayout();
            panelUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(bindingSourceUsers)).BeginInit();

            bindingSourceUsers.DataSource = users;
            panelUsers.Controls.Add(_gridUsers._groupBox);

            ((System.ComponentModel.ISupportInitialize)(bindingSourceUsers)).EndInit();
            panelUsers.ResumeLayout(false);
            panelUsers.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            using(FormUserAddEdit formUserAddEdit = new FormUserAddEdit())
                if (formUserAddEdit.ShowDialog() == DialogResult.OK)
                {
                    UserEntity user = formUserAddEdit._user;
                    user.Id = CreateUserId();

                    ICollection<UserEntity> allUsers = (ICollection<UserEntity>)bindingSourceUsers.DataSource;
                    allUsers.Add(user);

                    _gridUsers.Refresh();
                }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            ICollection<UserEntity> selectedUsers = new GridAssistant().GetCollection<UserEntity>(_gridUsers._gridData.SelectedRows);

            if( ! selectedUsers.Any()) new MessageBoxHelper().ShowWarningMessage("Пользователь не выбран.");
            if(selectedUsers.Count > 1) new MessageBoxHelper().ShowWarningMessage("Выберите только одного пользователя.");

            UserEntity selectedUser = selectedUsers.ElementAt(0);

            using(FormUserAddEdit formUserAddEdit = new FormUserAddEdit(selectedUser))
                if (formUserAddEdit.ShowDialog() == DialogResult.OK)
                {
                    UserEntity tempUser = formUserAddEdit._user;
                    PropertyInfo[] properties = typeof(UserEntity).GetProperties();

                    foreach(PropertyInfo property in properties)
                        if(property.CanWrite)
                            if("Id" != property.Name)
                                property.SetValue(selectedUser, property.GetValue(tempUser));

                    _gridUsers.Refresh();
                }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            ICollection<UserEntity> allUsers = (ICollection<UserEntity>)bindingSourceUsers.DataSource;
            ICollection<UserEntity> selectedUsers = new GridAssistant().GetCollection<UserEntity>(_gridUsers._gridData.SelectedRows);
            int lastId = selectedUsers.Count - 1;

            for (int u = lastId; u >= 0; -- u)
                allUsers.Remove(selectedUsers.ElementAt(u));

            _gridUsers.Refresh();
        }

        private void buttonActivate_Click(object sender, EventArgs e)
        {
            ICollection<UserEntity> selectedUsers = new GridAssistant().GetCollection<UserEntity>(_gridUsers._gridData.SelectedRows);

            foreach(UserEntity selectedUser in selectedUsers)
                selectedUser.Active = ! selectedUser.Active;

            _gridUsers.Refresh();
        }
    }
}
