﻿using GridWithFilter.HelpersToControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace GridWithFilter
{
    public class GridWithFilter<T>
    {
        public BindingSource _dataFake { get; private set; }

        private BindingSource _data;
        private List<ColumnsSettings> _columnsSettings;
        private int _gridFilterHeaderHeight;

        private int _sortColumnID = 0;
        private bool _sortByAsc = true;

        private string[] _filterNew,
                         _filterOld;

        private DataGridViewColumn[] _columnFilterArray,
                                     _columnDataArray;
        private List<Type> _columnType = new List<Type>();
        private List<PropertyInfo> _columnPropertyInfo = new List<PropertyInfo>();

        public GroupBox _groupBox = new GroupBox();
        public Panel _panelCommon = new Panel();
        public Panel _panelScrollCorner = new Panel();
        public HScrollBar _hScrollBarData = new HScrollBar();
        public VScrollBar _vScrollBarData = new VScrollBar();
        public Panel _panelSeparator = new Panel();
        public DataGridView _gridFilter = new DataGridView();
        public Panel _panelData = new Panel();
        public DataGridView _gridData { get; private set; }

        private const int _groupBoxAddWidht = 9,
                          _groupBoxBorderTop = 16,
                          _groupBoxBorderBottom = 3,
                          _panelAddHeight = 3,
                          _gridFilterAddHeight = 3;

        private bool _syncWidhtColumns = false;
        private bool _autoWidhColumnsExecute = false;

        public ContextMenuStrip _contextMenuStrip;
        private int _contextMenuColumnID;

        public class GridState
        {
            private ICollection<T> _selected;

            public void Save(DataGridView grid) =>
                _selected = new GridAssistant().GetCollection<T>(grid.SelectedRows);

            public void Restore(DataGridView grid)
            {
                GridAssistant gridAssistant = new GridAssistant();
                gridAssistant.DeselectAll(grid.SelectedRows);
                gridAssistant.SelectByCollection(grid.Rows, _selected);
            }
        }

        public class ColumnsSettings
        {
            internal string _fieldName,
                          _headerText;
            internal int _columnWidhtMin;
            internal object _nullValue;
            internal bool _visible;

            public ColumnsSettings(string fieldName, string headerText, int columnWidht, object nullValue = null, bool visible = true)
            {
                _fieldName = fieldName;
                _headerText = headerText;
                _columnWidhtMin = columnWidht;
                _nullValue = nullValue;
                _visible = visible;
            }
        }

        public GridWithFilter(
            ref BindingSource data,
            IContainer components,
            List<ColumnsSettings> columnsSettings,
            int gridFilterHeaderHeight,
            int sortColumnID)
        {
            _gridData = new DataGridView();

            _data = data;
            _dataFake = new BindingSource(components);
            _columnsSettings = columnsSettings;
            _gridFilterHeaderHeight = gridFilterHeaderHeight;
            _sortColumnID = sortColumnID;

            int countColumns = _columnsSettings.Count;

            for (int c = 0; c < countColumns; ++c)
            {
                _columnPropertyInfo.Add(typeof(T).GetProperty(columnsSettings[c]._fieldName));
                _columnType.Add(_columnPropertyInfo[c].PropertyType);
            }

            _filterNew = new string[countColumns];
            _filterOld = new string[countColumns];
            for (int c = 0; c < countColumns; ++c)
            {
                _filterNew[c] = string.Empty;
                _filterOld[c] = string.Empty;
            }
        }

        public void InitializeComponent(
            string groupBoxText,
            Point groupBoxLocation,
            int groupBoxHeight,
            int tabIndexFilter,
            int tabIndexData,
            DockStyle groupBoxDockStyle = DockStyle.Fill,
            DockStyle panelCommonDockStyle = DockStyle.Fill,
            AnchorStyles groupBoxAnchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
            AnchorStyles panelCommonAnchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
            bool gridDataMultiSelect = true,
            string uniqueName = "")
        {
            ((ISupportInitialize)(_data)).BeginInit();
            ((ISupportInitialize)(_dataFake)).BeginInit();
            ((ISupportInitialize)(_gridData)).BeginInit();
            ((ISupportInitialize)(_gridFilter)).BeginInit();
            _gridData.SuspendLayout();
            _panelData.SuspendLayout();
            _gridFilter.SuspendLayout();
            _panelSeparator.SuspendLayout();
            _panelScrollCorner.SuspendLayout();
            _panelCommon.SuspendLayout();
            _groupBox.SuspendLayout();

            int countColumns = _columnsSettings.Count;

            _columnFilterArray = new DataGridViewColumn[_columnsSettings.Count];
            _columnDataArray = new DataGridViewColumn[_columnsSettings.Count];
            for (int c = 0; c < countColumns; ++c)
                if (typeof(bool) == _columnType[c])
                {
                    DataGridViewCheckBoxColumn columnFilter = new DataGridViewCheckBoxColumn();
                    columnFilter.DataPropertyName = _columnsSettings[c]._fieldName;
                    columnFilter.HeaderText = _columnsSettings[c]._headerText;
                    columnFilter.Width = _columnsSettings[c]._columnWidhtMin;
                    columnFilter.MinimumWidth = _columnsSettings[c]._columnWidhtMin;
                    columnFilter.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    columnFilter.ReadOnly = false;
                    columnFilter.Name = $"{_columnsSettings[c]._fieldName}FilterTextBoxColumn{uniqueName}";
                    columnFilter.Visible = _columnsSettings[c]._visible;
                    columnFilter.ThreeState = true;
                    _columnFilterArray[c] = columnFilter;

                    DataGridViewCheckBoxColumn columnData = new DataGridViewCheckBoxColumn();
                    columnData.DataPropertyName = _columnsSettings[c]._fieldName;
                    columnData.HeaderText = _columnsSettings[c]._headerText;
                    columnData.Width = _columnsSettings[c]._columnWidhtMin;
                    columnData.MinimumWidth = _columnsSettings[c]._columnWidhtMin;
                    columnData.ReadOnly = true;
                    columnData.Name = $"{_columnsSettings[c]._fieldName}DataTextBoxColumn{uniqueName}";
                    columnData.Visible = _columnsSettings[c]._visible;
                    _columnDataArray[c] = columnData;
                }
                else
                {
                    DataGridViewTextBoxColumn columnFilter = new DataGridViewTextBoxColumn();
                    columnFilter.DataPropertyName = _columnsSettings[c]._fieldName;
                    columnFilter.HeaderText = _columnsSettings[c]._headerText;
                    columnFilter.Width = _columnsSettings[c]._columnWidhtMin;
                    columnFilter.MinimumWidth = _columnsSettings[c]._columnWidhtMin;
                    columnFilter.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    columnFilter.ReadOnly = false;
                    columnFilter.Name = $"{_columnsSettings[c]._fieldName}FilterTextBoxColumn{uniqueName}";
                    columnFilter.Visible = _columnsSettings[c]._visible;
                    _columnFilterArray[c] = columnFilter;

                    DataGridViewTextBoxColumn columnData = new DataGridViewTextBoxColumn();
                    columnData.DataPropertyName = _columnsSettings[c]._fieldName;
                    columnData.HeaderText = _columnsSettings[c]._headerText;
                    columnData.Width = _columnsSettings[c]._columnWidhtMin;
                    columnData.MinimumWidth = _columnsSettings[c]._columnWidhtMin;
                    columnData.ReadOnly = true;
                    columnData.Name = $"{_columnsSettings[c]._fieldName}DataTextBoxColumn{uniqueName}";
                    if (null != _columnsSettings[c]._nullValue)
                    {
                        DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
                        cellStyle.NullValue = _columnsSettings[c]._nullValue;
                        columnData.DefaultCellStyle = cellStyle;
                    }
                    columnData.Visible = _columnsSettings[c]._visible;
                    _columnDataArray[c] = columnData;
                }

            int widhtMinGrids = 0;
            foreach (ColumnsSettings columnSettings in _columnsSettings)
                if (columnSettings._visible)
                    widhtMinGrids += columnSettings._columnWidhtMin;

            // 
            // _gridFilter
            // 
            _gridFilter.Name = "_gridFilter{uniqueName}";
            List<object> dataSource = new List<object>() { new object() };
            _gridFilter.DataSource = dataSource;
            _gridFilter.AllowUserToAddRows = false;
            _gridFilter.AllowUserToDeleteRows = false;
            _gridFilter.AllowUserToResizeRows = false;
            _gridFilter.AutoGenerateColumns = false;
            _gridFilter.MultiSelect = false;
            _gridFilter.ReadOnly = false;
            _gridFilter.RowHeadersVisible = false;
            _gridFilter.SelectionMode = DataGridViewSelectionMode.CellSelect;
            _gridFilter.Columns.AddRange(_columnFilterArray);
            _gridFilter.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _gridFilter.Dock = DockStyle.None;
            _gridFilter.Location = new Point(0, 0);
            _gridFilter.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            _gridFilter.ColumnHeadersHeight = _gridFilterHeaderHeight;
            _gridFilter.ScrollBars = ScrollBars.None;
            int gridFilterPreferredSizeHeight = _gridFilter.PreferredSize.Height;
            _gridFilter.Size = new Size(widhtMinGrids, gridFilterPreferredSizeHeight + _gridFilterAddHeight);
            _gridFilter.CellMouseClick += new DataGridViewCellMouseEventHandler(GridEventCellMouseClick);
            _gridFilter.ColumnWidthChanged += new DataGridViewColumnEventHandler(GridFilterEventColumnWidthChanged);
            _gridFilter.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(FilterEventColumnHeaderMouseClick);
            _gridFilter.AllowUserToOrderColumns = true;
            _gridFilter.ColumnDisplayIndexChanged += new DataGridViewColumnEventHandler(GridFilterEventColumnDisplayIndexChanged);
            _gridFilter.EditMode = DataGridViewEditMode.EditOnEnter;
            _gridFilter.CellPainting += new DataGridViewCellPaintingEventHandler(FilterEventCellPainting);
            _gridFilter.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(FilterEventEditingControlShowing);
            _gridFilter.CellContentClick += new DataGridViewCellEventHandler(FilterEventCellContentClickDoubleClickForCheckBox);
            _gridFilter.CellContentDoubleClick += new DataGridViewCellEventHandler(FilterEventCellContentClickDoubleClickForCheckBox);
            _gridFilter.BorderStyle = BorderStyle.None;
            _gridFilter.TabIndex = tabIndexFilter;
            _gridFilter.TabStop = true;

            // 
            // _gridData
            // 
            _gridData.Name = "_gridData{uniqueName}";
            _gridData.DataSource = _dataFake;
            _gridData.AllowUserToAddRows = false;
            _gridData.AllowUserToDeleteRows = false;
            _gridData.AllowUserToResizeRows = false;
            _gridData.AutoGenerateColumns = false;
            _gridData.MultiSelect = gridDataMultiSelect;
            _gridData.ReadOnly = true;
            _gridData.RowHeadersVisible = false;
            _gridData.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            _gridData.Columns.AddRange(_columnDataArray);
            _gridData.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _gridData.Dock = DockStyle.None;
            _gridData.Location = new Point(0, 0);
            _gridData.ColumnHeadersVisible = false;
            _gridData.ScrollBars = ScrollBars.None;
            _gridData.Size = new Size(widhtMinGrids,
                groupBoxHeight - _groupBoxBorderTop - _groupBoxBorderBottom - _panelAddHeight
                - (gridFilterPreferredSizeHeight + _gridFilterAddHeight));
            _gridData.ColumnWidthChanged += new DataGridViewColumnEventHandler(GridDataEventColumnWidthChanged);
            _gridData.CellMouseClick += new DataGridViewCellMouseEventHandler(GridEventCellMouseClick);
            _gridData.BorderStyle = BorderStyle.None;
            _gridData.TabIndex = tabIndexData;
            _gridData.TabStop = true;
            // 
            // _panelData
            // 
            _panelData.Name = "_panelData{uniqueName}";
            _panelData.Controls.Add(_gridData);
            _panelData.AutoScroll = false;
            _panelData.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _panelData.Dock = DockStyle.None;
            _panelData.Location = new Point(0, gridFilterPreferredSizeHeight + _gridFilterAddHeight);
            _panelData.BorderStyle = BorderStyle.None;
            _panelData.Size = new Size(widhtMinGrids,
                groupBoxHeight - _groupBoxBorderTop - _groupBoxBorderBottom - _panelAddHeight
                - (gridFilterPreferredSizeHeight + _gridFilterAddHeight) + _panelAddHeight);
            // _panelData.TabIndex = 0;
            // _panelData.TabStop = false;

            // 
            // _panelSeparator
            // 
            _panelSeparator.Name = "_panelSeparator{uniqueName}";
            _panelSeparator.AutoScroll = false;
            _panelSeparator.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _panelSeparator.Dock = DockStyle.None;
            _panelSeparator.Location = new Point(0, gridFilterPreferredSizeHeight + _gridFilterAddHeight);
            _panelSeparator.BorderStyle = BorderStyle.None;
            _panelSeparator.Size = new Size(widhtMinGrids, 1);
            _panelSeparator.BackColor = Color.Black;
            // _panelSeparator.TabIndex = 0;
            // _panelSeparator.TabStop = false;
            // 
            // _hScrollBarData
            // 
            _hScrollBarData.Name = "_hScrollBarData{uniqueName}";
            _hScrollBarData.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            _hScrollBarData.Dock = DockStyle.None;
            _hScrollBarData.Scroll += new ScrollEventHandler(HScrollBarData_Scroll);
            _hScrollBarData.Visible = false;
            // _hScrollBarData.TabIndex = 0;
            // _hScrollBarData.TabStop = false;
            // 
            // _vScrollBarData
            // 
            _vScrollBarData.Name = "_vScrollBarData{uniqueName}";
            _vScrollBarData.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            _vScrollBarData.Dock = DockStyle.None;
            _vScrollBarData.Scroll += new ScrollEventHandler(VScrollBarData_Scroll);
            _vScrollBarData.Visible = false;
            _vScrollBarData.SmallChange = 22;
            // _vScrollBarData.TabIndex = 0;
            // _vScrollBarData.TabStop = false;
            // 
            // _panelScrollCorner
            // 
            _panelScrollCorner.Name = "_panelScrollCorner{uniqueName}";
            _panelScrollCorner.AutoScroll = false;
            _panelScrollCorner.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            _panelScrollCorner.Dock = DockStyle.None;
            _panelScrollCorner.BorderStyle = BorderStyle.None;
            _panelScrollCorner.Size = new Size(17, 17);
            _panelScrollCorner.Visible = false;
            // _panelScrollCorner.TabIndex = 0;
            // _panelScrollCorner.TabStop = false;

            // 
            // _panelCommon
            // 
            _panelCommon.Name = "_panelCommon{uniqueName}";
            _panelCommon.Controls.Add(_panelScrollCorner);
            _panelCommon.Controls.Add(_vScrollBarData);
            _panelCommon.Controls.Add(_hScrollBarData);
            _panelCommon.Controls.Add(_panelSeparator);
            _panelCommon.Controls.Add(_gridFilter);
            _panelCommon.Controls.Add(_panelData);
            _panelCommon.AutoScroll = false;
            _panelCommon.Anchor = panelCommonAnchor;
            _panelCommon.Dock = panelCommonDockStyle;
            _panelCommon.Location = new Point(_groupBoxBorderBottom, _groupBoxBorderTop);
            _panelCommon.BorderStyle = BorderStyle.FixedSingle;
            _panelCommon.Size = new Size(widhtMinGrids, groupBoxHeight - _groupBoxBorderTop - _groupBoxBorderBottom);
            _panelCommon.SizeChanged += new EventHandler(PanelCommonEventSizeChanged);
            _panelCommon.MouseWheel += new MouseEventHandler(PanelCommonEventMouseWheel);
            // _panelCommon.TabIndex = 0;
            // _panelCommon.TabStop = false;

            // 
            // _groupBox
            // 
            _groupBox.Text = groupBoxText;
            _groupBox.Name = "_groupBox{uniqueName}";
            _groupBox.Controls.Add(_panelCommon);
            _groupBox.Anchor = groupBoxAnchor;
            _groupBox.Dock = groupBoxDockStyle;
            _groupBox.Location = groupBoxLocation;
            _groupBox.Size = new Size(widhtMinGrids + _groupBoxAddWidht, groupBoxHeight);
            // _groupBox.TabIndex = 0;
            // _groupBox.TabStop = false;

            ((ISupportInitialize)(_data)).EndInit();
            ((ISupportInitialize)(_dataFake)).EndInit();
            ((ISupportInitialize)(_gridData)).EndInit();
            ((ISupportInitialize)(_gridFilter)).EndInit();
            _gridData.ResumeLayout();
            _panelData.ResumeLayout(false);
            _gridFilter.ResumeLayout();
            _panelSeparator.ResumeLayout(false);
            _panelScrollCorner.ResumeLayout(false);
            _panelCommon.ResumeLayout(false);
            _groupBox.ResumeLayout(false);
        }

        /// <summary>
        /// Старт.
        /// </summary>
        private void FilterEventCellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            ((DataGridView)sender).CellPainting -= FilterEventCellPainting;

            int countColumns = _gridFilter.ColumnCount;
            for (int c = 0; c < countColumns; ++c)
                if (typeof(bool) == _columnType[c])
                {
                    ((DataGridViewCheckBoxCell)_gridFilter.Rows[0].Cells[c]).EditingCellFormattedValue = CheckState.Checked;
                    _gridFilter.Rows[0].Cells[c].Value = CheckState.Checked;
                    _filterOld[c] = _filterNew[c] = true.ToString();
                }
            ExecuteFilter();
            Sort();
            new GridAssistant().DeselectAll(_gridData.SelectedRows);
            if (_gridData.Rows.Count > 0)
                _gridData.Rows[0].Selected = true;

            ScrollManagerExecute();
            AutoWidthColumnsExecute();
        }

        #region Сортировка.
        private void FilterEventColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button)
                return;

            if (e.ColumnIndex == _sortColumnID)
                _sortByAsc = ! _sortByAsc;
            else
            {
                _sortColumnID = e.ColumnIndex;
                _sortByAsc = true;
            }

            Sort();
        }

        public void Sort()
        {
            GridState gridState = new GridState();
            gridState.Save(_gridData);

            PropertyInfo propertyInfo = _columnPropertyInfo[_sortColumnID];
            if (_sortByAsc)
            {
                _data.DataSource = ((List<T>)_data.DataSource).OrderBy(x => propertyInfo.GetValue(x, null)).ToList();
                if (null != _dataFake.DataSource)
                    _dataFake.DataSource = ((List<T>)_dataFake.DataSource).OrderBy(x => propertyInfo.GetValue(x, null)).ToList();
            }
            else
            {
                _data.DataSource = ((List<T>)_data.DataSource).OrderByDescending(x => propertyInfo.GetValue(x, null)).ToList();
                if (null != _dataFake.DataSource)
                    _dataFake.DataSource = ((List<T>)_dataFake.DataSource).OrderByDescending(x => propertyInfo.GetValue(x, null)).ToList();
            }

            _data.ResetBindings(false);
            _dataFake.ResetBindings(false);

            gridState.Restore(_gridData);
        }
        #endregion

        #region Фильтрация.
        private void FilterEventEditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (_columnType[((DataGridView)sender).CurrentCell.ColumnIndex] != typeof(bool))
            {
                DataGridViewTextBoxEditingControl control = e.Control as DataGridViewTextBoxEditingControl;
                if (null != control)
                {
                    control.TextChanged -= FilterEventTextChanged;
                    control.TextChanged += FilterEventTextChanged;
                }
            }
        }

        private void FilterEventTextChanged(object sender, EventArgs e)
        {
            _filterNew[_gridFilter.CurrentCell.ColumnIndex] = ((DataGridViewTextBoxEditingControl)sender).Text;
            CheckFilter();
        }

        private void FilterEventCellContentClickDoubleClickForCheckBox(object sender, DataGridViewCellEventArgs e)
        {
            if (0 == e.RowIndex && typeof(bool) == _columnType[e.ColumnIndex])
            {
                _filterNew[e.ColumnIndex] = CheckBoxSwitch((DataGridView)sender, e.ColumnIndex).ToString();
                CheckFilter();
            }
        }

        private bool? CheckBoxSwitch(DataGridView gridFilter, int columnID)
        {
            bool? result = null;
            CheckState checkState = (CheckState)gridFilter.Rows[0].Cells[columnID].Value;

            switch (checkState)
            {
                case CheckState.Checked:
                    result = null;
                    checkState = CheckState.Indeterminate;
                    break;
                case CheckState.Indeterminate:
                    result = false;
                    checkState = CheckState.Unchecked;
                    break;
                case CheckState.Unchecked:
                    result = true;
                    checkState = CheckState.Checked;
                    break;
            }

            gridFilter.Rows[0].Cells[columnID].Value = checkState;

            return result;
        }

        private void CheckFilter()
        {
            int countColumns = _filterNew.Length;

            for (int c = 0; c < countColumns; ++c)
                if (_filterOld[c] != _filterNew[c])
                {
                    ExecuteFilter();
                    break;
                }

            for (int c = 0; c < countColumns; ++c)
                _filterOld[c] = _filterNew[c];
        }

        public void Refresh() => ExecuteFilter();

        public void ExecuteFilter()
        {
            GridState gridState = new GridState();
            gridState.Save(_gridData);

            int countColumns = _filterNew.Length;

            List<T> result = new List<T>();
            List<T> rowsList = new List<T>((List<T>)_data.DataSource);
            object cell;

            bool isMatch;
            string filterNew;

            int countRows = rowsList.Count,
                c;
            for (int r = 0; r < countRows; ++r)
            {
                isMatch = true;

                for (c = 0; c < countColumns; ++c)
                {
                    filterNew = _filterNew[c];
                    cell = _columnPropertyInfo[c].GetValue(rowsList[r], null);

                    if( ! (typeof(bool) == _columnType[_sortColumnID] && (string.IsNullOrEmpty(filterNew) || cell.ToString().ToUpper().Contains(filterNew.ToUpper())))
                    &&(null != cell && ! string.IsNullOrEmpty(filterNew) && ! cell.ToString().ToUpper().Contains(filterNew.ToUpper())
                    || null == cell && null == _columnsSettings[c]._nullValue && ! string.IsNullOrEmpty(filterNew)
                    || null == cell && null != _columnsSettings[c]._nullValue && ! _columnsSettings[c]._nullValue.ToString().ToUpper().Contains(filterNew.ToUpper())))
                        isMatch = false;
                }

                if (isMatch)
                    result.Add(rowsList[r]);
            }

            _dataFake.DataSource = result;

            ScrollManagerExecute();
            AutoWidthColumnsExecute();

            gridState.Restore(_gridData);
        }
        #endregion

        #region Контекстное меню - скрытие и показ столбцов.
        private void GridEventCellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            _contextMenuStrip = new ContextMenuStrip();

            if (MouseButtons.Right == e.Button)
            {
                DataGridView grid = (DataGridView)sender;
                HideColumnContextMenuItem(_contextMenuStrip, grid);
                ShowColumnContextMenuItem(_contextMenuStrip, _gridFilter);
                GetLocationContextMenu(grid, e, out int x, out int y);
                _contextMenuColumnID = e.ColumnIndex;
                _contextMenuStrip.Show(grid, x, y);
            }
        }

        private void HideColumnContextMenuItem(ContextMenuStrip contextMenuStrip, DataGridView grid)
        {
            ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem("Скрыть столбец");
            if (1 == GetVisibleColumnsCount(grid))
                toolStripMenuItem.Enabled = false;
            toolStripMenuItem.Click += EventHideColumnContextMenuItem;
            contextMenuStrip.Items.Add(toolStripMenuItem);
        }

        private int GetVisibleColumnsCount(DataGridView grid)
        {
            int result = 0,
                count = grid.ColumnCount;
            for (int i = 0; i < count; ++i)
                if (grid.Columns[i].Visible)
                    result++;
            return result;
        }

        private void EventHideColumnContextMenuItem(object sender, EventArgs e)
        {
            if(_contextMenuColumnID < _gridData.ColumnCount
            && _contextMenuColumnID < _gridFilter.ColumnCount)
            {
                _gridData.Columns[_contextMenuColumnID].Visible = false;
                _gridFilter.Columns[_contextMenuColumnID].Visible = false;

                ScrollManagerExecute();
                AutoWidthColumnsExecute();
            }
        }

        private void ShowColumnContextMenuItem(ContextMenuStrip contextMenuStrip, DataGridView grid)
        {
            ToolStripMenuItem showColumnItem = new ToolStripMenuItem("Отобразить столбец");
            contextMenuStrip.Items.Insert(0, showColumnItem);
            showColumnItem.Enabled = false;
            ContextMenuStrip contextMenuStripShowColumn = new ContextMenuStrip();
            bool first = true;
            int countColumn = grid.ColumnCount;
            for (int c = 0; c < countColumn; ++c)
                if (false == grid.Columns[c].Visible)
                {
                    if (first)
                    {
                        first = false;
                        showColumnItem.Enabled = true;
                        showColumnItem.DropDown = contextMenuStripShowColumn;
                    }

                    ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(grid.Columns[c].HeaderText);
                    toolStripMenuItem.Click += EventShowColumnContextMenuItem;
                    contextMenuStripShowColumn.Items.Add(toolStripMenuItem);
                }
        }

        private void EventShowColumnContextMenuItem(object sender, EventArgs e)
        {
            int countColumn = _gridData.ColumnCount;
            for (int showColumn = 0; showColumn < countColumn; ++showColumn)
                if (_gridFilter.Columns[showColumn].HeaderText.Equals(((ToolStripItem)sender).Text))
                {
                    _gridData.Columns[showColumn].Visible = true;
                    _gridFilter.Columns[showColumn].Visible = true;

                    SyncWidht();
                    ScrollManagerExecute();

                    if (_gridData.Columns[_contextMenuColumnID].DisplayIndex < _gridData.Columns[showColumn].DisplayIndex)
                    {
                        _gridData.Columns[showColumn].DisplayIndex = _gridData.Columns[_contextMenuColumnID].DisplayIndex;
                        _gridFilter.Columns[showColumn].DisplayIndex = _gridFilter.Columns[_contextMenuColumnID].DisplayIndex;
                    }
                    else
                    {
                        _gridData.Columns[showColumn].DisplayIndex = _gridData.Columns[_contextMenuColumnID].DisplayIndex - 1;
                        _gridFilter.Columns[showColumn].DisplayIndex = _gridFilter.Columns[_contextMenuColumnID].DisplayIndex - 1;
                    }

                    break;
                }
        }

        private void GetLocationContextMenu(DataGridView grid, DataGridViewCellMouseEventArgs e, out int x, out int y)
        {
            x = e.X;
            y = e.Y;

            int countColumns = grid.ColumnCount;
            int columnIndex = e.ColumnIndex,
                rowIndex = e.RowIndex;

            if (grid.RowHeadersVisible && columnIndex >= 0)
                y += grid.RowHeadersWidth;
            for (int i = 0; i < countColumns; ++i)
                if (grid.Columns[i].Visible)
                    if (grid.Columns[i].DisplayIndex < grid.Columns[columnIndex].DisplayIndex)
                        x += grid.Columns[i].Width;

            if (grid.ColumnHeadersVisible && rowIndex >= 0)
                y += grid.ColumnHeadersHeight;
            for (int i = 0; i < rowIndex; ++i)
                if (grid.Rows[i].Visible)
                    y += grid.Rows[i].Height;
        }
        #endregion

        /// <summary>
        /// Перестановка столбцов.
        /// </summary>
        private void GridFilterEventColumnDisplayIndexChanged(object sender, DataGridViewColumnEventArgs e) =>
            _gridData.Columns[e.Column.Index].DisplayIndex = _gridFilter.Columns[e.Column.Index].DisplayIndex;

        private void PanelCommonEventSizeChanged(object sender, EventArgs e)
        {
            ScrollManagerExecute();
            AutoWidthColumnsExecute();
        }

        #region Скроллы.
        private void PanelCommonEventMouseWheel(object sender, MouseEventArgs e)
        {
            if(_vScrollBarData.Visible)
            {
                int newValueY;
                if(e.Delta < 0)
                {
                    newValueY = _vScrollBarData.Value + _vScrollBarData.SmallChange;
                    int max = _vScrollBarData.Maximum - _panelData.Height + 1;
                    if(newValueY > max)
                        newValueY = max;
                }
                else
                {
                     newValueY = _vScrollBarData.Value - _vScrollBarData.SmallChange;
                    if(newValueY < _vScrollBarData.Minimum)
                        newValueY = _vScrollBarData.Minimum;
                }
                _vScrollBarData.Value = newValueY;
                SetGridLocation(_gridData, new Point(_gridData.Location.X, -newValueY));
            }
        }
        private void SetGridLocation(DataGridView grid, Point point)
        {
            grid.Location = new Point(point.X, point.Y);
            grid.Refresh();
        }

        private void ScrollManagerExecute()
        {
            ScrollVerticalVisibleSwitch(_panelCommon, _gridData, _gridFilter, _gridFilterHeaderHeight);
            ScrollHorizontalVisibleSwitch(_panelCommon, _gridData, _gridFilter);
            ScrollVerticalVisibleSwitch(_panelCommon, _gridData, _gridFilter, _gridFilterHeaderHeight);
            ScrollHorizontalVisibleSwitch(_panelCommon, _gridData, _gridFilter);

            ScrollHorizontalConctructor();
            ScrollVerticalConctructor();

            ScrollHorizontalClingsDataWhenPulledRight();
            ScrollVerticalClingsDataWhenPulledDown();

            _panelScrollCorner.Location = new Point(_panelCommon.Width - 19, _panelCommon.Height - 19);
            _panelScrollCorner.Visible = _hScrollBarData.Visible && _vScrollBarData.Visible;
        }

        private void HScrollBarData_Scroll(object sender, ScrollEventArgs e)
        {
            SetGridLocation(_gridFilter, new Point(-e.NewValue, _gridFilter.Location.Y));
            SetGridLocation(_gridData, new Point(-e.NewValue, _gridData.Location.Y));
        }
        private void VScrollBarData_Scroll(object sender, ScrollEventArgs e) =>
            SetGridLocation(_gridData, new Point(_gridData.Location.X, -e.NewValue));

        private void ScrollHorizontalConctructor()
        {
            int OffsetVerticalScroll = (_vScrollBarData.Visible ? 19 : 0);
            _hScrollBarData.Width = _panelCommon.Width - OffsetVerticalScroll + (_vScrollBarData.Visible ? 2 : 0) - 2;
            _hScrollBarData.Location = new Point(0, _panelCommon.Height - _hScrollBarData.Height - 2);
            _hScrollBarData.Maximum = GetWidthGrid(_gridData) + OffsetVerticalScroll - (_vScrollBarData.Visible ? 2 : 0) + 2;
            _hScrollBarData.LargeChange = _panelData.Width;
        }
        private void ScrollVerticalConctructor()
        {
            int OffsetHorizontalScroll = (_hScrollBarData.Visible ? 19 : 0);
            _vScrollBarData.Height = _panelCommon.Height - OffsetHorizontalScroll + (_hScrollBarData.Visible ? 2 : 0) - 2;
            _vScrollBarData.Location = new Point(_panelCommon.Width - _vScrollBarData.Width - 2, 0);
            _vScrollBarData.Maximum = GetHeightGrid(_gridData) + OffsetHorizontalScroll - (_hScrollBarData.Visible ? 2 : 0) + 2;
            _vScrollBarData.LargeChange = _panelData.Height;
        }

        private void ScrollHorizontalClingsDataWhenPulledRight()
        {
            int widthHiddenPartOfGrid = -_gridData.Location.X,
                widthVisiblePartOfGrid = _gridData.Width - widthHiddenPartOfGrid,
                widthOfVoidInPanel = _panelData.Width - widthVisiblePartOfGrid;

            widthOfVoidInPanel = widthOfVoidInPanel - (_vScrollBarData.Visible ? 19 : 0) + (_vScrollBarData.Visible ? 2 : 0) - 2;

            widthOfVoidInPanel = widthOfVoidInPanel > 0 ? widthOfVoidInPanel : 0;

            int LocationX = _gridData.Location.X +
                (widthHiddenPartOfGrid < widthOfVoidInPanel ? widthHiddenPartOfGrid : widthOfVoidInPanel);

            _gridFilter.Location = new Point(LocationX, _gridFilter.Location.Y);
            _gridData.Location = new Point(LocationX, _gridData.Location.Y);
        }
        private void ScrollVerticalClingsDataWhenPulledDown()
        {
            int heightHiddenPartOfGrid = -_gridData.Location.Y,
                heightVisiblePartOfGrid = _gridData.Height - heightHiddenPartOfGrid,
                heightOfVoidInPanel = _panelData.Height - heightVisiblePartOfGrid;

            heightOfVoidInPanel = heightOfVoidInPanel - (_hScrollBarData.Visible ? 19 : 0) + (_hScrollBarData.Visible ? 2 : 0) - 2;

            heightOfVoidInPanel = heightOfVoidInPanel > 0 ? heightOfVoidInPanel : 0;

            int LocationY = _gridData.Location.Y +
                (heightHiddenPartOfGrid < heightOfVoidInPanel ? heightHiddenPartOfGrid : heightOfVoidInPanel);

            _gridData.Location = new Point(_gridData.Location.X, LocationY);
        }

        private void ScrollHorizontalVisibleSwitch(Panel panel, DataGridView gridData, DataGridView gridFilter)
        {
            int widhtPanel = panel.Width - (_vScrollBarData.Visible ? 19 : 0),
                widhtGrids = GetWidthGrid(gridFilter);
            if (widhtPanel <= widhtGrids + 2 - (_vScrollBarData.Visible ? 2 : 0))
                _hScrollBarData.Visible = true;
            else
            {
                _hScrollBarData.Value = 0;

                _hScrollBarData.Visible = false;
            }
            gridData.Width =
            gridFilter.Width = widhtGrids + 1;
        }
        private void ScrollVerticalVisibleSwitch(Panel panel, DataGridView gridData, DataGridView gridFilter, int gridFilterHeaderHeight)
        {
            if (0 == gridData.Rows.Count)
                return;

            int heightPanel = panel.Height - (_hScrollBarData.Visible ? 19 : 0),
                heightGridFilter = gridFilterHeaderHeight + gridFilter.Rows[0].Height,
                heightGridData = GetHeightGrid(gridData);
            int heightGrids = heightGridFilter + heightGridData;
            if (heightPanel <= heightGrids + 1 - (_hScrollBarData.Visible ? 2 : 0))
            {
                /*
                Вертикальный скролл панели может быть виден не зависимо от Visible и Enabled
                при видимости горизонтального скролла
                и определённых настройках Anchor элементов входящих в ту же панель,
                тем самым перекрывая отдельный скролл.
                */
                _vScrollBarData.Visible = true;
                gridData.Height = heightGridData + 1;
            }
            else
            {
                _vScrollBarData.Value = 0;

                _vScrollBarData.Visible = false;
                gridData.Height = heightPanel - heightGridFilter + 1;
            }
        }

        private int GetWidthGrid(DataGridView grid)
        {
            int result = grid.RowHeadersVisible ? grid.RowHeadersWidth : 0,
                countColumns = grid.ColumnCount;
            for (int c = 0; c < countColumns; ++c)
                if (grid.Columns[c].Visible)
                    result += grid.Columns[c].Width;
            return result;
        }
        private int GetHeightGrid(DataGridView grid)
        {
            int result = grid.ColumnHeadersVisible ? grid.ColumnHeadersHeight : 0,
                countRows = grid.RowCount;
            for (int c = 0; c < countRows; ++c)
                if (grid.Rows[c].Visible)
                    result += grid.Rows[c].Height;
            return result;
        }
        #endregion

        #region Изменение ширины столбцов.
        private void GridFilterEventColumnWidthChanged(object sender, DataGridViewColumnEventArgs e) =>
            EventColumnWidthChanged(e.Column.Index, _gridData, _gridFilter);

        private void GridDataEventColumnWidthChanged(object sender, DataGridViewColumnEventArgs e) =>
            EventColumnWidthChanged(e.Column.Index, _gridFilter, _gridData);

        private void EventColumnWidthChanged(int columnIndex, DataGridView grid1, DataGridView grid2)
        {
            if (_syncWidhtColumns) return;
            _syncWidhtColumns = true;

            SyncWidhtColumns(grid1, grid2);
            SyncWidht();
            ScrollManagerExecute();
            AutoWidthColumnsExecute(columnIndex);

            _syncWidhtColumns = false;
        }
        #endregion

        internal class ColumnWidhtFill
        {
            public int _index,
                       _dispayIndex;
            public double _rounded;

            public ColumnWidhtFill(int index, int dispayIndex, double rounded)
            {
                _index = index;
                _dispayIndex = dispayIndex;
                _rounded = rounded;
            }
        }

        private void AutoWidthColumnsExecute(int skipColumnIndex = -1)
        {
            if (_autoWidhColumnsExecute) return;
            _autoWidhColumnsExecute = true;

            int lacksWidth = (_panelCommon.Width - 2 - (_vScrollBarData.Visible ? 17 : 0)) - (GetWidthGrid(_gridData) + 1);
            if (lacksWidth > 0)
            {
                DataGridViewColumnCollection columns = _gridData.Columns;
                DataGridViewColumn column;
                List<ColumnWidhtFill> columnWidhtFillList = new List<ColumnWidhtFill>();

                int countColumns = columns.Count,
                    widhtColumns = GetWidthGrid(_gridData) - (-1 != skipColumnIndex ? columns[skipColumnIndex].Width : 0),
                    balance = lacksWidth,
                    incrementInt;
                double incrementDouble;

                for (int c = 0; c < countColumns; ++c)
                {
                    column = columns[c];

                    if (column.Visible
                    && skipColumnIndex != c)
                    {
                        incrementDouble = (double)lacksWidth / widhtColumns * column.Width;
                        incrementInt = (int)incrementDouble;
                        column.Width += incrementInt;
                        balance -= incrementInt;
                        columnWidhtFillList.Add(new ColumnWidhtFill(column.Index, column.DisplayIndex, incrementDouble - incrementInt));
                    }
                }

                if (balance > 0)
                {
                    columnWidhtFillList = columnWidhtFillList.OrderByDescending(c => c._dispayIndex).ToList();
                    columnWidhtFillList = columnWidhtFillList.OrderByDescending(c => c._rounded).ToList();

                    for (int b = 0; b < balance; ++b)
                        columns[columnWidhtFillList[b]._index].Width++;
                }

                SyncWidhtColumns(_gridFilter, _gridData);
                SyncWidht();
            }

            _autoWidhColumnsExecute = false;
        }

        private void SyncWidhtColumns(DataGridView grid1, DataGridView grid2)
        {
            int countColumns = grid1.ColumnCount;
            for (int c = 0; c < countColumns; ++c)
                grid1.Columns[c].Width = grid2.Columns[c].Width;
        }

        private void SyncWidht() =>
            _gridData.Width = _gridFilter.Width = GetWidthGrid(_gridData) + 1;
    }
}
