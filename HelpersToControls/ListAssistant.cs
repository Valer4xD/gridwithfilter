﻿using System.Collections.Generic;
using System.Windows.Forms;
using static System.Windows.Forms.ListBox;

namespace GridWithFilter.HelpersToControls
{
    internal class ListAssistant
    {
        internal ICollection<T> GetCollection<T>(SelectedObjectCollection selectedItems)
        {
            ICollection<T> collection = new List<T>();
            foreach (T item in selectedItems)
                collection.Add(item);
            return collection;
        }

        internal IEnumerable<T> SelectByCollection<T>(ListBox listBox, IEnumerable<T> collection)
        {
            T item;
            ObjectCollection items = listBox.Items;
            int count = items.Count;
            for (int i = 0; i < count; ++i)
            {
                item = (T)items[i];
                foreach (T requiredSelectItem in collection)
                    if (item.Equals(requiredSelectItem))
                    {
                        listBox.SetSelected(items.IndexOf(item), value: true);
                        break;
                    }
            }
            return collection;
        }
        internal void DeselectAll(ListBox listBox)
        {
            ObjectCollection items = listBox.Items;
            int count = items.Count;
            for (int i = 0; i < count; ++i)
                listBox.SetSelected(i, value: false);
        }
    }
}
