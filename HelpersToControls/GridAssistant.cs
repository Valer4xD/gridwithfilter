﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace GridWithFilter.HelpersToControls
{
    internal class GridAssistant
    {
        internal void SelectionCorrectionAfterDelete(DataGridView dataGridView)
        {
            if (dataGridView.SelectedRows.Count == 0
            && dataGridView.Rows.Count != 0)
            {
                int lastRowID = dataGridView.Rows.Count - 1;
                dataGridView.Rows[lastRowID].Selected = true;
            }
        }

        internal ICollection<T> GetCollection<T>(DataGridViewSelectedRowCollection selectedRows)
        {
            ICollection<T> collection = new List<T>();
            foreach (DataGridViewRow row in selectedRows)
                collection.Add((T)row.DataBoundItem);
            return collection;
        }

        internal IEnumerable<T> SelectByCollection<T>(DataGridViewRowCollection rows, IEnumerable<T> collection)
        {
            foreach (DataGridViewRow row in rows)
                foreach (T requiredSelectItem in collection)
                    if (((T)row.DataBoundItem).Equals(requiredSelectItem))
                    {
                        row.Selected = true;
                        break;
                    }
            return collection;
        }
        internal void DeselectAll(DataGridViewSelectedRowCollection selectedRows)
        {
            foreach (DataGridViewRow row in selectedRows)
                row.Selected = false;
        }
    }
}
