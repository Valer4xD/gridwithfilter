﻿using GridWithFilter.HelpersToControls;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Forms;

namespace GridWithFilter
{
    internal partial class FormUserAddEdit : Form
    {
        internal FormUserAddEdit(UserEntity user = null)
        {
            InitializeComponent();

            if(null != user)
            {
                checkBoxIsActive.Checked = user.Active;
                textBoxSurname.Text = user.Surname;
                textBoxName.Text = user.Name;
                textBoxMiddleName.Text = user.MiddleName;
                maskedTextBoxDateOfBirth.Text = user.DateOfBirth.ToString();
            }
        }

        internal UserEntity _user;
        private void buttonOk_Click(object sender, EventArgs e)
        {
            DateTime.TryParse(maskedTextBoxDateOfBirth.Text, out DateTime dateOfBirth);

            _user = new UserEntity(
                id: 0,
                active: checkBoxIsActive.Checked,
                surname: textBoxSurname.Text,
                name: textBoxName.Text,
                middleName: textBoxMiddleName.Text,
                dateOfBirth: dateOfBirth);

            ICollection<ValidationResult> validationResults = _user.GetValidationResults();
            if( ! validationResults.Any())
                DialogResult = DialogResult.OK;
            else
            {
                MessageBoxHelper messageBoxHelper = new MessageBoxHelper();
                foreach(ValidationResult validationResult in validationResults)
                    messageBoxHelper.ShowErrorMessage(validationResult.ErrorMessage);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e) => DialogResult = DialogResult.Cancel;
    }
}
